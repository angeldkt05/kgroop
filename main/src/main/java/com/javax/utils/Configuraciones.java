package com.javax.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuraciones 
{
	Properties propiedades;
	FileInputStream config;
	String Direccion = "https://danny-experiments.appspot.com/alpha/api/%method%?InputType=json&MimeType=application/json&api-key=%key%";
	
	public Configuraciones() throws IOException 
	{
		propiedades = new Properties();
        config = new FileInputStream("config.xml");
        propiedades.loadFromXML(config);
	}
	
	public String getMemberDetails(){
		return Direccion.replace("%method%", propiedades.getProperty("method.GetMemberDetails")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String getMemberBenefits(){
		return Direccion.replace("%method%", propiedades.getProperty("method.GetMemberBenefits")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String getRedeem(){
		return Direccion.replace("%method%", propiedades.getProperty("method.Redeem")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String getSubmitPurchase(){
		return Direccion.replace("%method%", propiedades.getProperty("method.SubmitPurchase")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String getCancelPurchase(){
		return Direccion.replace("%method%", propiedades.getProperty("method.CancelPurchase")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String PayWithBudget(){
		return Direccion.replace("%method%", propiedades.getProperty("method.PayWithBudget")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	public String getCancelBudgetPayment(){
		return Direccion.replace("%method%", propiedades.getProperty("method.CancelBudgetPayment")).replace("%key%", propiedades.getProperty("url.key"));
	}
	
	
	
}
